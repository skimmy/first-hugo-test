## Playground site

This site is used to familiarize with [Hugo](https://gohugo.io/) and does not
really contain any useful information (unless you need to learn something on
Hugo by looking at the source code).

{{% red "Ignore **this**" %}}

{{< alert "About Me" "warning" >}}
I'd like to use **markdown** and the {{< red "**red** shortcode" >}}
together, with not much success!
{{< /alert >}}

{{< alert "Example" "success" >}}
This should be green and if not there is some serious issue with the
``alert`` shortcode (or with the bootstrap ``CSS`` inclusion)
{{< /alert >}}

Similarly to the ``alert`` shortcode, a ``colored`` shortcode can be used
to color text according to Bootstrap ``text-type`` colors
* {{< colored "primary" "Primary" >}}
* {{< colored "secondary" "Secondary" >}}
* {{< colored "danger" "Danger" >}}

**If you are interested in my projects visit my [GitLab profile](https://gitlab.com/skimmy)**

